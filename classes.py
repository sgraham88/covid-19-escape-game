#!/bin/python3

################
# COVID ESCAPE #
################
# Silas Graham #
#     2020     #
################

#---
#Game Classes
#---

import turtle
import math
import random
import time

#Register shapes
images = ["character_right.gif", "character_left.gif", "character_up.gif", 
"character_down.gif","covid_green.gif", "clorox.gif",
"treasure.gif", "wall.gif", "slime.gif", "door.gif", "heart.gif",
"key2.gif", "wiz_r_d8.gif"]

for image in images:
	turtle.register_shape(image)

#Create Pen
class Pen(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.shape("square") 
        self.color("white")
        self.penup()
        self.speed(0)

#Create Text
class Text(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.color("white")
        self.penup()
        self.speed(0)
        self.hideturtle()

#Create Legend
class Legend(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.penup()
        self.speed(0)
        self.hideturtle()

    def draw_background(self):
    	self.goto(320, 300)
    	self.pendown()
    	self.pensize(5)
    	self.color("white", "grey")
    	self.begin_fill()
    	for i in range(4):
    		if (i % 2) == 0:
    			self.forward(200)
    		else:
    			self.forward(250)
    		self.right(90)
    	self.end_fill()
    	self.penup()
    	self.goto(330, 250)
    	self.color("white")
    	self.pensize(2)
    	self.pendown()
    	self.forward(180)
    	self.penup()
    	self.goto(410, 260)
    	self.write("Player 1", align="center", font=("Pixel Digivolve",18,"normal"))

    def legend_treasure(self, player):
    	self.color("gold")
    	self.goto(340,220)
    	self.write("Gold:", align="left", font=("Pixel Digivolve",16,"normal"))
    	self.goto(440,220)
    	self.write(player.gold, align="left", font=("Pixel Digivolve",16,"normal"))

    def legend_health(self, player):
    	self.color("red")
    	self.goto(340,180)
    	self.write("Lives:", align="left", font=("Pixel Digivolve",16,"normal"))
    	self.goto(445,190)
    	self.shape("heart.gif")
    	i = player.lives
    	while i >= 1:
    		self.stamp()
    		self.forward(16)
    		i -= 1

    def legend_clorox(self, player):
    	self.color("white")
    	self.shape("clorox.gif")
    	self.goto(340,140)
    	self.write("Clorox:", align="left", font=("Pixel Digivolve",16,"normal"))
    	self.goto(445,150)
    	c = player.clorox
    	while c >= 1:
    		self.stamp()
    		self.forward(18)
    		c -= 1

    def legend_key(self, player):
    	self.color("green")
    	self.goto(340,100)
    	self.write("Keys:", align="left", font=("Pixel Digivolve",16,"normal"))
    	self.goto(445,110)
    	self.shape("key2.gif")
    	if player.key is True:
    		self.stamp()

    def legend_update(self, player):
    	self.clear()
    	self.draw_background()
    	self.legend_treasure(player)
    	self.legend_health(player)
    	self.legend_clorox(player)
    	self.legend_key(player)


#Create PLayer Class
class Player(turtle.Turtle):
	def __init__(self):
		turtle.Turtle.__init__(self)
		self.shape("character_right.gif")
		self.color("blue")
		self.penup()
		self.speed(0)
		self.gold = 0
		self.lives = 3
		self.clorox = 3
		self.can_move = True
		self.dead = False
		self.invincible = False
		self.key = False

	def go_up(self):
		#Calculate the spot to move to
		move_to_x = player.xcor()
		move_to_y = player.ycor() + 24

		if self.dead is False:
			self.shape("character_up.gif")

		#Check if there is a wall
		if (move_to_x,move_to_y) not in walls and self.can_move is True:
			self.goto(move_to_x, move_to_y)

	def go_down(self):
		#Calculate the spot to move to
		move_to_x = player.xcor()
		move_to_y = player.ycor() - 24

		if self.dead is False:
			self.shape("character_down.gif")

		#Check if there is a wall
		if (move_to_x,move_to_y) not in walls and self.can_move is True:
			self.goto(move_to_x, move_to_y)


	def go_left(self):
		#Calculate the spot to move to
		move_to_x = player.xcor() - 24
		move_to_y = player.ycor()

		if self.dead is False:
			self.shape("character_left.gif")

		#Check if there is a wall
		if (move_to_x,move_to_y) not in walls and self.can_move is True:
			self.goto(move_to_x, move_to_y)

	def go_right(self):
		#Calculate the spot to move to
		move_to_x = player.xcor() + 24
		move_to_y = player.ycor()

		if self.dead is False:
			self.shape("character_right.gif")

		#Check if there is a wall
		if (move_to_x,move_to_y) not in walls and self.can_move is True:
			self.goto(move_to_x, move_to_y)

	def is_collision(self, other):
		a = self.xcor()-other.xcor()
		b = self.ycor()-other.ycor()
		distance = math.sqrt((a ** 2) + (b ** 2))

		if distance < 5:
			return True
		else:
			return False

	def place_clorox(self):
		if self.clorox != 0:
			placed_clorox = Clorox()
			placed_clorox.place(self)

			#add placed clorox coordinates to a list
			cloroxes.append(placed_clorox)
			legend.legend_update(self)

	def hit(self):
		self.goto(self.start_x, self.start_y)
		print("Player Hit!")

	def death(self):
		#self.shape("wiz_right_death.gif")
		self.can_move = False
		self.dead = True
		self.shape("wiz_r_d8.gif")
		self.stamp()
		print("Player Died!")
		self.goto(0,0)
		self.color("red")
		self.write("Game Over", False, align="center", font=("Pixel Digivolve",36,"normal"))


#Create Treasure Class
class Treasure(turtle.Turtle):
	def __init__(self, x, y):
		turtle.Turtle.__init__(self)
		self.shape("treasure.gif")
		self.penup()
		self.speed(0)
		self.gold = 100
		self.goto(x, y)

	def destroy(self):
		self.goto(2000, 2000)
		self.hideturtle()

#Create Key Class
class Key(turtle.Turtle):
	def __init__(self, x, y):
		turtle.Turtle.__init__(self)
		self.shape("key2.gif")
		self.penup()
		self.speed(0)
		self.goto(x, y)

	def destroy(self):
		self.goto(2000, 2000)
		self.hideturtle()

#Create Door Class
class Door(turtle.Turtle):
	def __init__(self, x, y):
		turtle.Turtle.__init__(self)
		self.shape("door.gif")
		self.penup()
		self.speed(0)
		self.is_locked = True
		self.goto(x, y)

	def destroy(self):
		self.goto(2000, 2000)
		self.hideturtle()

#Create Clorox Class
class Clorox(turtle.Turtle):
	def __init__(self):
		turtle.Turtle.__init__(self)
		self.shape("clorox.gif")
		self.penup()
		self.speed(0)

	def place(self, player):
		x = player.xcor()
		y = player.ycor()
		if player.clorox != 0:
			self.goto(x,y)
			self.stamp()
			player.clorox -= 1

	def enemy_collide(self, enemy):
		#check if there is a collision between the clorox and enemy
		if (enemy.xcor(), enemy.ycor()) == (self.xcor(), self.ycor()):
			return True
			#enemy.destroy()
			#self.destroy()

	def destroy(self):
		self.clearstamps()
		self.goto(2000, 2000)
		self.hideturtle()

#Create Enemy Class
class Enemy(turtle.Turtle):
	def __init__(self, x, y):
		turtle.Turtle.__init__(self)
		self.shape("covid_green.gif")
		self.penup()
		self.speed(0)
		self.gold = 25
		self.goto(x, y)
		self.direction = random.choice(["up", "down", "left", "right"])
		self.collide = False

	def is_collision(self, others):
		for other in range(len(others)):
			a = self.xcor()-others[other][0]
			b = self.ycor()-others[other][1]
			distance = math.sqrt((a ** 2) + (b ** 2))

			if distance < 1:
				return True
			else:
				return False

	def move(self):
		if self.direction == "up":
			dx = 0
			dy = 24
		elif self.direction == "down":
			dx = 0
			dy = -24
		elif self.direction == "left":
			dx = -24
			dy = 0
		elif self.direction == "right":
			dx = 24
			dy = 0
		else:
			dx = 0
			dy = 0

		#Calculate the spot to move to
		move_to_x = self.xcor() + dx
		move_to_y = self.ycor() + dy

		#Check if the space has a wall
		if (move_to_x, move_to_y) not in walls:
			self.goto(move_to_x, move_to_y)
		else:
			#Choose a different direction
			self.direction = random.choice(["up", "down", "left", "right"])

		#Set timet to move next time
		turtle.ontimer(self.move, t=random.randint(100,300))

	def destroy(self):
		self.goto(2000, 2000)
		self.hideturtle()
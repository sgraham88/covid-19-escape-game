#!/bin/python3

################
# COVID ESCAPE #
################
# Silas Graham #
#     2020     #
################

#---
#Game functions and classes
#---

import turtle
import math
import random
import threading
from levels import levels

WINDOW_WIDTH = 1100
WINDOW_HEIGHT = 700

wn = turtle.Screen()
wn.bgcolor("black")
wn.title("A Maze Game")
wn.setup(WINDOW_WIDTH,WINDOW_HEIGHT)
wn.tracer(0) #makes the map show up fast

#Register shapes
images = ["character_right.gif", "character_left.gif", "character_up.gif", 
"character_down.gif","covid_green.gif", "clorox.gif", "clorox_refill.gif",
"treasure.gif", "wall.gif", "slime.gif", "door.gif", "heart.gif",
"key2.gif", "wiz_r_d8.gif"]

for image in images:
	turtle.register_shape(image)

#Create Pen
class Pen(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.shape("square") 
        self.color("white")
        self.penup()
        self.speed(0)

#Create Text
class Text(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.color("white")
        self.penup()
        self.speed(0)
        self.hideturtle()

#Create Legend
class Legend(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.penup()
        self.speed(0)
        self.hideturtle()

    def draw_background(self):
    	self.goto(320, 300)
    	self.pendown()
    	self.pensize(5)
    	self.color("white", "grey")
    	self.begin_fill()
    	for i in range(4):
    		if (i % 2) == 0:
    			self.forward(200)
    		else:
    			self.forward(250)
    		self.right(90)
    	self.end_fill()
    	self.penup()
    	self.goto(330, 250)
    	self.color("white")
    	self.pensize(2)
    	self.pendown()
    	self.forward(180)
    	self.penup()
    	self.goto(410, 260)
    	self.write("Player 1", align="center", font=("Pixel Digivolve",18,"normal"))

    def legend_treasure(self, player):
    	self.color("gold")
    	self.goto(340,220)
    	self.write("Gold:", align="left", font=("Pixel Digivolve",16,"normal"))
    	self.goto(440,220)
    	self.write(player.gold, align="left", font=("Pixel Digivolve",16,"normal"))

    def legend_health(self, player):
    	self.color("red")
    	self.goto(340,180)
    	self.write("Lives:", align="left", font=("Pixel Digivolve",16,"normal"))
    	self.goto(445,190)
    	self.shape("heart.gif")
    	i = player.lives
    	while i >= 1:
    		self.stamp()
    		self.forward(16)
    		i -= 1

    def legend_clorox(self, player):
    	self.color("white")
    	self.shape("clorox.gif")
    	self.goto(340,140)
    	self.write("Clorox:", align="left", font=("Pixel Digivolve",16,"normal"))
    	self.goto(445,150)
    	c = player.clorox
    	while c >= 1:
    		self.stamp()
    		self.forward(18)
    		c -= 1

    def legend_key(self, player):
    	self.color("green")
    	self.goto(340,100)
    	self.write("Keys:", align="left", font=("Pixel Digivolve",16,"normal"))
    	self.goto(445,110)
    	self.shape("key2.gif")
    	if player.key is True:
    		self.stamp()

    def legend_update(self, player):
    	self.clear()
    	self.draw_background()
    	self.legend_treasure(player)
    	self.legend_health(player)
    	self.legend_clorox(player)
    	self.legend_key(player)


#Create PLayer Class
class Player(turtle.Turtle):
	def __init__(self):
		turtle.Turtle.__init__(self)
		self.shape("character_right.gif")
		self.color("blue")
		self.penup()
		self.speed(0)
		self.gold = 0
		self.lives = 3
		self.clorox = 3
		self.can_move = True
		self.dead = False
		self.invincible = False
		self.key = False

	def go_up(self):
		#Calculate the spot to move to
		move_to_x = player.xcor()
		move_to_y = player.ycor() + 24

		if self.dead is False:
			self.shape("character_up.gif")

		#Check if there is a wall
		if (move_to_x,move_to_y) not in walls and self.can_move is True:
			self.goto(move_to_x, move_to_y)

	def go_down(self):
		#Calculate the spot to move to
		move_to_x = player.xcor()
		move_to_y = player.ycor() - 24

		if self.dead is False:
			self.shape("character_down.gif")

		#Check if there is a wall
		if (move_to_x,move_to_y) not in walls and self.can_move is True:
			self.goto(move_to_x, move_to_y)


	def go_left(self):
		#Calculate the spot to move to
		move_to_x = player.xcor() - 24
		move_to_y = player.ycor()

		if self.dead is False:
			self.shape("character_left.gif")

		#Check if there is a wall
		if (move_to_x,move_to_y) not in walls and self.can_move is True:
			self.goto(move_to_x, move_to_y)

	def go_right(self):
		#Calculate the spot to move to
		move_to_x = player.xcor() + 24
		move_to_y = player.ycor()

		if self.dead is False:
			self.shape("character_right.gif")

		#Check if there is a wall
		if (move_to_x,move_to_y) not in walls and self.can_move is True:
			self.goto(move_to_x, move_to_y)

	def is_collision(self, other):
		a = self.xcor()-other.xcor()
		b = self.ycor()-other.ycor()
		distance = math.sqrt((a ** 2) + (b ** 2))

		if distance < 5:
			return True
		else:
			return False

	def place_clorox(self):
		if self.clorox != 0:
			placed_clorox = Clorox()
			placed_clorox.place(self)

			#add placed clorox coordinates to a list
			cloroxes.append(placed_clorox)
			legend.legend_update(self)

	#Give the player a 2 second invicibility mode when reset, to get out of spawn
	def invincible_reset(self):
		if self.invincible == False:
			print("Player invincible")
			self.invincible = True
			timer = threading.Timer(2.0, self.invincible_reset)
			timer.start()
		else:
			self.invincible = False

	def hit(self):
		self.goto(self.start_x, self.start_y)
		self.invincible_reset()
		print("Player Hit!")

	def death(self):
		#self.shape("wiz_right_death.gif")
		self.can_move = False
		self.dead = True
		self.shape("wiz_r_d8.gif")
		self.stamp()
		print("Player Died!")
		self.goto(0,0)
		self.color("red")
		self.write("Game Over", False, align="center", font=("Pixel Digivolve",36,"normal"))


#Create Treasure Class
class Treasure(turtle.Turtle):
	def __init__(self, x, y):
		turtle.Turtle.__init__(self)
		self.shape("treasure.gif")
		self.penup()
		self.speed(0)
		self.gold = 100
		self.goto(x, y)

	def destroy(self):
		self.goto(2000, 2000)
		self.hideturtle()

#Create Key Class
class Key(turtle.Turtle):
	def __init__(self, x, y):
		turtle.Turtle.__init__(self)
		self.shape("key2.gif")
		self.penup()
		self.speed(0)
		self.goto(x, y)

	def destroy(self):
		self.goto(2000, 2000)
		self.hideturtle()

#Create Door Class
class Door(turtle.Turtle):
	def __init__(self, x, y):
		turtle.Turtle.__init__(self)
		self.shape("door.gif")
		self.penup()
		self.speed(0)
		self.is_locked = True
		self.goto(x, y)

	def destroy(self):
		self.goto(2000, 2000)
		self.hideturtle()

#Create Door Class
class Clorox_Refill(turtle.Turtle):
	def __init__(self, x, y):
		turtle.Turtle.__init__(self)
		self.shape("clorox_refill.gif")
		self.penup()
		self.speed(0)
		self.goto(x, y)

	def destroy(self):
		self.goto(2000, 2000)
		self.hideturtle()

#Create Clorox Class
class Clorox(turtle.Turtle):
	def __init__(self):
		turtle.Turtle.__init__(self)
		self.shape("clorox.gif")
		self.penup()
		self.speed(0)

	def place(self, player):
		x = player.xcor()
		y = player.ycor()
		if player.clorox != 0:
			self.goto(x,y)
			self.stamp()
			player.clorox -= 1

	def enemy_collide(self, enemy):
		#check if there is a collision between the clorox and enemy
		if (enemy.xcor(), enemy.ycor()) == (self.xcor(), self.ycor()):
			return True
			#enemy.destroy()
			#self.destroy()

	def destroy(self):
		self.clearstamps()
		self.goto(2000, 2000)
		self.hideturtle()

#Create Enemy Class
class Enemy(turtle.Turtle):
	def __init__(self, x, y):
		turtle.Turtle.__init__(self)
		self.shape("covid_green.gif")
		self.penup()
		self.speed(0)
		self.gold = 25
		self.goto(x, y)
		self.direction = random.choice(["up", "down", "left", "right"])
		self.collide = False

	def is_collision(self, others):
		for other in range(len(others)):
			a = self.xcor()-others[other][0]
			b = self.ycor()-others[other][1]
			distance = math.sqrt((a ** 2) + (b ** 2))

			if distance < 1:
				return True
			else:
				return False

	def move(self):
		if self.direction == "up":
			dx = 0
			dy = 24
		elif self.direction == "down":
			dx = 0
			dy = -24
		elif self.direction == "left":
			dx = -24
			dy = 0
		elif self.direction == "right":
			dx = 24
			dy = 0
		else:
			dx = 0
			dy = 0

		#Calculate the spot to move to
		move_to_x = self.xcor() + dx
		move_to_y = self.ycor() + dy

		#Check if the space has a wall
		if (move_to_x, move_to_y) not in walls:
			self.goto(move_to_x, move_to_y)
		else:
			#Choose a different direction
			self.direction = random.choice(["up", "down", "left", "right"])

		#Set timet to move next time
		turtle.ontimer(self.move, t=random.randint(100,300))

	def destroy(self):
		self.goto(2000, 2000)
		self.hideturtle()

def draw_rules(t):
    t.goto(-530, 300)
    t.pendown()
    t.pensize(5)
    t.color("white", "grey")
    t.begin_fill()
    for i in range(4):
    	if (i % 2) == 0:
    		t.forward(210)
    	else:
    		t.forward(350)
    	t.right(90)
    t.end_fill()
    t.penup()
    t.goto(-515, 250)
    t.color("white")
    t.pensize(2)
    t.pendown()
    t.forward(185)
    t.penup()
    t.goto(-420, 260)
    t.write("Rules", align="center", font=("Pixel Digivolve",18,"normal"))
    t.goto(-520, 65)
    t.write("You are in self-isolation, " +
    	"\nand must escape the " + 
    	"\nCOVID-19!" + 
    	"\nDon't forget to pickup" + 
    	"\na key to unlock the door," +
    	"\nand collect as much TP " + 
    	"\nas you can!"
    	"\nUse Clorox to kill the" +
    	"\nCOVIDS!" + 
    	"\n\nWill you make it outside?",
    	 align="left", font=("Pixel Digivolve",10,"italic"))
    t.goto(-515, 50)
    t.pendown()
    t.forward(185)
    t.penup()
    t.goto(-520, 20)
    t.write("Move:  Arrow Keys", align="left", font=("Pixel Digivolve",10,"normal"))
    t.goto(-520, -20)
    t.write("Place Clorox Sheet:  \n	   Spacebar", align="left", font=("Pixel Digivolve",10,"normal"))
    t.goto(-520, -40)
    t.write("Quit Game:  q", align="left", font=("Pixel Digivolve",10,"normal"))

def kill_program():
	turtle.bye()


#Add a treasures list
treasures = []

#Add keys to a list
keys = []

#Add doors to a list
doors = []

#Add clorox refill bottles to a list
clorox_refill = []

#Add enemies to a list
enemies = []

#Create Level Setup Function
def setup_maze(level):
	for y in range(len(level)):
		for x in range(len(level[y])):
			#Get the character at each x, y coordinate
			#NOTE the order of y and x in the next line
			character = level[y][x]
			#Calculate the screen x, y coordinates
			screen_x = -288 + (x * 24)
			screen_y = 288 - (y * 24)

			#Check if it is an X (representing a wall)
			if character == "X":
				pen.goto(screen_x, screen_y)
				pen.shape("wall.gif")
				pen.stamp()
				#Add coordinates to wall list
				walls.append((screen_x, screen_y))
			#Check if it is a P (representing the player)
			elif character == "P":
				player.goto(screen_x, screen_y)
				player.start_x = screen_x
				player.start_y = screen_y
			#Check if it is a T (representing a treasure)
			elif character == "T":
				treasures.append(Treasure(screen_x, screen_y))
			#Check if it is a K (representing key)
			elif character == "K":
				keys.append(Key(screen_x, screen_y))
			#Check if it is a D (representing the door)
			elif character == "D":
				doors.append(Door(screen_x, screen_y))
			#Check if it is a C (representing Clorox refill)
			elif character ==  "C":
				clorox_refill.append(Clorox_Refill(screen_x, screen_y))
			#Check if it is an E (representing an enemy)
			elif character == "E":
				enemies.append(Enemy(screen_x, screen_y))


	for enemy in enemies:
		enemy.forward(0)


#Create class instances
pen = Pen()
title = Text()
rules = Text()
player = Player()
legend = Legend()
legend.legend_update(player)

title.penup()
title.goto(0, 300)
title.write("COVID ESCAPE - A GAME", False, align="center", font=("Pixel Digivolve", 24, "italic"))
draw_rules(rules)

#Create wall coordinate list
walls = []

#Create clorox coordinate list
cloroxes = []

#Set up the level
current_level = 1
setup_maze(levels[current_level])

#Keyboard Binding
turtle.listen()
turtle.onkey(player.go_left,"Left")
turtle.onkey(player.go_right,"Right")
turtle.onkey(player.go_up,"Up")
turtle.onkey(player.go_down,"Down")
turtle.onkey(player.place_clorox," ")
turtle.onkey(kill_program,"q")

# turtle.onkey(player2.go_left,"a")
# turtle.onkey(player2.go_right,"d")
# turtle.onkey(player2.go_up,"w")
# turtle.onkey(player2.go_down,"s")


#Turn off screen updates
wn.tracer(0)

#Start moving enemies
for enemy in enemies:
	turtle.ontimer(enemy.move, t=250)

#Main Game Loop
while True:
	#check for the player collision with treasure
	#Iterate through the treasure list
	for treasure in treasures:
		if player.is_collision(treasure):
			#Add the treasure gold to the player gold
			player.gold += treasure.gold
			print("Player Gold: {}".format(player.gold))
			legend.legend_update(player)
			#Destroy the treasure
			treasure.destroy()
			#Remove the treasure from the treasures list
			treasures.remove(treasure)

	#check for collision with key(s)
	for key in keys:
		if player.is_collision(key):
			player.key = True
			key.destroy()
			#unlock the doors
			for door in doors:
				door.is_locked = False
			legend.legend_update(player) #update the legend

	#check for collision with clorox refill bottle
	for refill in clorox_refill:
		if player.is_collision(refill):
			if player.clorox < 3:
				player.clorox = 3
				legend.legend_update(player)
				refill.destroy()
				clorox_refill.remove(refill)


	#Check for player collision with door. If door is unlocked,
	#then load the next level
	for door in doors:
		if player.is_collision(door):
			if door.is_locked is False:

				#Move to the next level, clear all current level items
				for enemy in enemies:
					enemy.destroy()
				for treasure in treasures:
					treasure.destroy()
				for door in doors:
					door.destroy()
				for key in keys:
					key.destroy()

				#Clear existing walls, enemies, etc. from screen
				walls.clear()
				treasures.clear()
				doors.clear()
				enemies.clear()
				keys.clear()
				pen.clear()

				player.key = False

				current_level += 1 #Proceed to the next level
				if current_level > len(levels)-1: #Final Level = WIN!
					#Won!
					player.goto(0,0)
					player.color("green")
					player.write("You Win!", False, align="center", font=("Pixel Digivolve",42,"normal"))
					break
				else: #Setup the next level
					setup_maze(levels[current_level])
					#Start moving enemies
					for enemy in enemies:
						turtle.ontimer(enemy.move, t=250)

				legend.legend_update(player) #update the legend

	#Check for enemy collisions with placed clorox
	for clorox in cloroxes:
		for enemy in enemies:
			if clorox.enemy_collide(enemy):
				player.gold += enemy.gold #Add gold for killing enemy
				#Destroy enemy and clorox
				enemy.destroy()
				enemies.remove(enemy) #remove enemy from enemies list
				clorox.destroy()
				cloroxes.remove(clorox) #remove clorox from cloroxes list
				legend.legend_update(player) #update legend to reflect new gold

	#Check for enemy collision with player
	for enemy in enemies:
		if player.is_collision(enemy):
			if not player.invincible: #if the player is invincible, no collision
				player.lives -= 1 #remove a life to a hit
				if player.lives > 0:
					player.hit() #if lives are remaining, lower players lives by 1
				elif player.lives == 0:
					player.death() #if no more lives remain, player is dead, and game is over
				else:
					break
			legend.legend_update(player) #update legend to show remaining lives
			
	#Update screen
	wn.update()
import turtle
#Create Player
class Player(turtle.Turtle):
	def __init__(self):
		turtle.Turtle.__init__(self)
		self.shape("wizard_right.gif")
		self.color("blue")
		self.penup()
		self.speed(0)
		self.gold = 0
		self.lives = 1
		self.can_move = True
		self.dead = False

	def go_up(self):
		#Calculate the spot to move to
		move_to_x = player.xcor()
		move_to_y = player.ycor() + 24

		#Check if there is a wall
		if (move_to_x,move_to_y) not in walls and self.can_move is True:
			self.goto(move_to_x, move_to_y)

	def go_down(self):
		#Calculate the spot to move to
		move_to_x = player.xcor()
		move_to_y = player.ycor() - 24

		#Check if there is a wall
		if (move_to_x,move_to_y) not in walls and self.can_move is True:
			self.goto(move_to_x, move_to_y)

	def go_left(self):
		#Calculate the spot to move to
		move_to_x = player.xcor() -24
		move_to_y = player.ycor()

		if self.dead is False:
			self.shape("wizard_left.gif")

		#Check if there is a wall
		if (move_to_x,move_to_y) not in walls and self.can_move is True:
			self.goto(move_to_x, move_to_y)

	def go_right(self):
		#Calculate the spot to move to
		move_to_x = player.xcor() + 24
		move_to_y = player.ycor()

		if self.dead is False:
			self.shape("wizard_right.gif")

		#Check if there is a wall
		if (move_to_x,move_to_y) not in walls and self.can_move is True:
			self.goto(move_to_x, move_to_y)

	def is_collision(self, other):
		a = self.xcor()-other.xcor()
		b = self.ycor()-other.ycor()
		distance = math.sqrt((a ** 2) + (b ** 2))

		if distance < 5:
			return True
		else:
			return False

	def hit(self):
		self.goto(self.start_x, self.start_y)
		print("Player Hit!")


	def death(self):
		#self.shape("wiz_right_death.gif")
		self.can_move = False
		self.dead = True
		self.shape("wiz_r_d8.gif")
		self.stamp()
		print("Player Died!")
		self.goto(0,0)
		self.color("red")
		self.write("Game Over", False, align="center", font=("Consolas",36,"bold"))

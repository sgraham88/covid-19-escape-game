#!/bin/python3

################
# COVID ESCAPE #
################
# Silas Graham #
#     2020     #
################

#---
#Level Editor
#---

import turtle
from classes import *

WINDOW_WIDTH = 1100
WINDOW_HEIGHT = 700

wn = turtle.Screen()
wn.bgcolor("black")
wn.title("A Maze Game")
wn.setup(WINDOW_WIDTH,WINDOW_HEIGHT)
wn.tracer(0) #makes the map show up fast

#Register shapes
images = ["character_right.gif", "character_left.gif", "character_up.gif", 
"character_down.gif","covid_green.gif", "clorox.gif", "clorox_refill.gif",
"treasure.gif", "wall.gif", "slime.gif", "door.gif", "heart.gif",
"key2.gif", "wiz_r_d8.gif"]

for image in images:
	turtle.register_shape(image)

#Create Pen
class Cursor(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.shape("square") 
        self.color("white")
        self.penup()
        self.speed(0)

    def go_up(self):
    	if self.ycor() < 288:
    		self.goto(self.xcor(), self.ycor() + 24)

    def go_down(self):
    	if self.ycor() > -288:
    		self.goto(self.xcor(), self.ycor() - 24)

    def go_left(self):
    	if self.xcor() > -288:
    		self.goto(self.xcor() - 24, self.ycor())

    def go_right(self):
    	if self.xcor() < 288:
    		self.goto(self.xcor() + 24, self.ycor())

    def black(self):
    	self.shape("square")
    	self.color("black")
    	self.stamp()

    def player(self):
    	self.shape("character_right.gif")
    	self.stamp()
    
    def key(self):
    	self.shape("key2.gif")
    	self.stamp()

    def door(self):
    	self.shape("door.gif")
    	self.stamp()

    def enemy(self):
    	self.shape("covid_green.gif")
    	self.stamp()

    def treasure(self):
    	self.shape("treasure.gif")
    	self.stamp()

    def clorox(self):
    	self.shape("character_right.gif")
    	self.stamp()

    def wall(self):
    	self.shape("wall.gif")
    	self.stamp()

    def mouseclick(self):
    	print((self.xcor(), self.ycor()))

#Create Pen
class Pen(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        #self.shape("square") 
        self.color("white")
        self.penup()
        self.speed(0)

    def change_cursor(self, input):
    	pass




def kill_program():
	turtle.bye()

level = []

#Class Instances
cursor = Cursor()
pen = Pen()

pen.hideturtle()
pen.goto(-300, 300)
pen.pendown()
for i in range(4):
	pen.forward(600)
	pen.right(90)
pen.penup()

#Keyboard Mapping
turtle.listen()
turtle.onkey(kill_program, "q")
turtle.onkey(cursor.go_left, "Left")
turtle.onkey(cursor.go_right, "Right")
turtle.onkey(cursor.go_up, "Up")
turtle.onkey(cursor.go_down, "Down")
turtle.onkey(cursor.black, " ")
turtle.onkey(cursor.wall, "1")
turtle.onkey(cursor.treasure, "2")
turtle.onkey(cursor.enemy, "3")
turtle.onkey(cursor.key, "4")
turtle.onkey(cursor.clorox, "5")
turtle.onkey(cursor.door, "6")

#Add Mouse clicking options
wn.onclick(cursor.goto)


while True:
	wn.update()
	pass
